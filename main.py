from problem import problem
from solution import solution

if __name__ == "__main__":
    print(solution(*problem()))
